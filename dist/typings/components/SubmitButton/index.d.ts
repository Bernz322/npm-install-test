import React from "react";
import { ButtonProps } from "@mui/material";
export interface BtnProps extends ButtonProps {
    inProgress?: boolean;
    text?: string;
    invalid?: boolean;
    darker?: boolean;
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    textTransform?: "none" | "lowercase" | "capitalize";
}
declare const SubmitButton: ({ text, classes, inProgress, invalid, onClick, darker, variant, color, sx, size, textTransform, ...props }: BtnProps) => JSX.Element;
export default SubmitButton;
