/// <reference types="react" />
import { BoxProps } from "@mui/material";
declare const Box: ({ children, ...props }: BoxProps) => JSX.Element;
export default Box;
