'use strict';

var _objectWithoutProperties = require('@babel/runtime/helpers/objectWithoutProperties');
var React = require('react');
var material = require('@mui/material');
var _extends = require('@babel/runtime/helpers/extends');
var _defineProperty = require('@babel/runtime/helpers/defineProperty');

var _excluded$1 = ["children"];
var Box = function Box(_ref) {
  var children = _ref.children,
    props = _objectWithoutProperties(_ref, _excluded$1);
  return /*#__PURE__*/React.createElement(material.Box, props, children);
};
var Box$1 = Box;

var _excluded = ["text", "classes", "inProgress", "invalid", "onClick", "darker", "variant", "color", "sx", "size", "textTransform"];
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }
var SubmitButton = function SubmitButton(_ref) {
  var _ref$text = _ref.text,
    text = _ref$text === void 0 ? "Submit" : _ref$text;
    _ref.classes;
    var _ref$inProgress = _ref.inProgress,
    inProgress = _ref$inProgress === void 0 ? false : _ref$inProgress,
    invalid = _ref.invalid,
    onClick = _ref.onClick,
    darker = _ref.darker,
    _ref$variant = _ref.variant,
    variant = _ref$variant === void 0 ? "contained" : _ref$variant,
    color = _ref.color,
    sx = _ref.sx,
    _ref$size = _ref.size,
    size = _ref$size === void 0 ? "medium" : _ref$size,
    textTransform = _ref.textTransform,
    props = _objectWithoutProperties(_ref, _excluded);
  var loaderSize = function loaderSize(btnSize) {
    switch (btnSize) {
      case "small":
        return 22;
      case "medium":
        return 28;
      default:
        return 38;
    }
  };
  var btnWidth = function btnWidth(btnSize) {
    switch (btnSize) {
      case "small":
        return 32;
      case "medium":
        return 38;
      default:
        return 48;
    }
  };
  return /*#__PURE__*/React.createElement(StyledButton, _extends({
    fullWidth: true,
    variant: variant,
    sx: _objectSpread({
      backgroundColor: !invalid && darker ? "#001a88" : "",
      "&:hover": {
        backgroundColor: !invalid && darker ? "#001a88" : ""
      },
      textTransform: textTransform
    }, sx),
    color: color,
    disabled: invalid,
    onClick: onClick,
    inProgress: inProgress,
    size: size,
    btnSize: size,
    btnWidth: btnWidth(size)
  }, props), inProgress ? /*#__PURE__*/React.createElement(material.CircularProgress, {
    color: "inherit",
    thickness: 5,
    size: loaderSize(size)
  }) : text);
};
var StyledButton = material.styled(material.Button, {
  shouldForwardProp: function shouldForwardProp(prop) {
    return prop !== "inProgress" && prop !== "btnSize" && prop !== "btnWidth";
  }
})(function (_ref2) {
  var inProgress = _ref2.inProgress,
    btnSize = _ref2.btnSize,
    btnWidth = _ref2.btnWidth;
  return {
    alignItems: "center",
    padding: btnSize === "large" && !inProgress ? "auto" : 5,
    maxWidth: inProgress ? 0 : "auto",
    minWidth: inProgress ? btnWidth : "auto",
    height: "auto !important",
    borderRadius: inProgress ? "50%" : "",
    fontSize: btnSize === "small" ? "0.8125rem" : "1rem"
  };
});
var SubmitButton$1 = SubmitButton;

exports.Box = Box$1;
exports.SubmitButton = SubmitButton$1;
