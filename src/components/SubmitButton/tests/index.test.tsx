import { render } from "@testing-library/react";
import SubmitButton from "..";

describe("first", () => {
  it("should render and match the snapshot", () => {
    const {
      container: { firstChild },
    } = render(<SubmitButton />);
    expect(firstChild).toMatchSnapshot();
  });
});
