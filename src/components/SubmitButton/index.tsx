import React from "react";
import { CircularProgress, Button, ButtonProps, styled } from "@mui/material";

export interface BtnProps extends ButtonProps {
  // Status of the button if it is in progress or not
  inProgress?: boolean;
  // Text to be rendered inside the button
  text?: string;
  // Disables button
  invalid?: boolean;
  // Darker variant of the button
  darker?: boolean;
  // onClick event handler, put it inside a useCallback hook to avoid unnecessary re-renders
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
  // Change button's text case
  textTransform?: "none" | "lowercase" | "capitalize";
}

const SubmitButton = ({
  text = "Submit",
  classes,
  inProgress = false,
  invalid,
  onClick,
  darker,
  variant = "contained",
  color,
  sx,
  size = "medium",
  textTransform,
  ...props
}: BtnProps) => {
  const loaderSize = (btnSize: BtnProps["size"]) => {
    switch (btnSize) {
      case "small":
        return 22;
      case "medium":
        return 28;
      default:
        return 38;
    }
  };

  const btnWidth = (btnSize: BtnProps["size"]) => {
    switch (btnSize) {
      case "small":
        return 32;
      case "medium":
        return 38;
      default:
        return 48;
    }
  };

  return (
    <StyledButton
      fullWidth
      variant={variant}
      sx={{
        backgroundColor: !invalid && darker ? "#001a88" : "",
        "&:hover": {
          backgroundColor: !invalid && darker ? "#001a88" : "",
        },
        textTransform,
        ...sx,
      }}
      color={color}
      disabled={invalid}
      onClick={onClick}
      inProgress={inProgress}
      size={size}
      btnSize={size}
      btnWidth={btnWidth(size)}
      {...props}
    >
      {inProgress ? (
        <CircularProgress
          color="inherit"
          thickness={5}
          size={loaderSize(size)}
        />
      ) : (
        text
      )}
    </StyledButton>
  );
};

const StyledButton = styled(Button, {
  shouldForwardProp: (prop) =>
    prop !== "inProgress" && prop !== "btnSize" && prop !== "btnWidth",
})<{ inProgress: boolean; btnSize: BtnProps["size"]; btnWidth: number }>(
  ({ inProgress, btnSize, btnWidth }) => ({
    alignItems: "center",
    padding: btnSize === "large" && !inProgress ? "auto" : 5,
    maxWidth: inProgress ? 0 : "auto",
    minWidth: inProgress ? btnWidth : "auto",
    height: "auto !important",
    borderRadius: inProgress ? "50%" : "",
    fontSize: btnSize === "small" ? "0.8125rem" : "1rem",
  })
);

export default SubmitButton;
