import React from "react";
import { Box as MUIBox, BoxProps } from "@mui/material";

const Box = ({ children, ...props }: BoxProps) => {
  return <MUIBox {...props}>{children}</MUIBox>;
};

export default Box;
